from setuptools import setup, find_packages
import gapex

setup(
    name='gapex',
    version=gapex.__version__,
    author='Geoff Johnson',
    author_email='geoff.johnson@coanda.ca',
    description='Apex Gtk user interface',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
    ],
    scripts=[
        'bin/gapex',
    ],
    # setup_requires=[
    #     'pytest-runner',
    # ],
    tests_require=[
        'pytest',
    ],
)
