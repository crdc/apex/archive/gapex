import gi

gi.require_version("Gtk", "3.0")

from gi.repository import GLib, Gtk
from .gi_composites import GtkTemplate

# FIXME: couldn't figure this out with resource:///...

@GtkTemplate(ui='data/ui/window.ui')
class GApexWindow(Gtk.ApplicationWindow):
    __gtype_name__ = 'GApexWindow'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.init_template()

    @GtkTemplate.Callback
    def on_destroy(self, user_data):
        print('unnecessary?')

    @GtkTemplate.Callback
    def on_row_get_configuration_activate(self, user_data):
        print('get-configuration')

    @GtkTemplate.Callback
    def on_row_get_status_activate(self, user_data):
        print('get-status')

    @GtkTemplate.Callback
    def on_btn_send_clicked(self, user_data):
        print('send request')
